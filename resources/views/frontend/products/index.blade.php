@extends('layouts.front')

@section('title')
    {{$category->name}}
@endsection

@section('content')
    <div class="py-3 mb-4 shadow-sm bg-warning border-top">
        <div class="container">
            <h6 class="mb-0">
                <a href="{{url('category') }}">
                    Collections
                </a> / {{$category->name}}
            </h6>
        </div>
    </div>
    <div class="py-5">
        <div class="container">
            <div class="row">
                <h4>{{$category->name}}</h4>
                @foreach($products as $product)
                    <div class="col-md-3 mb-3">
                        <a href="{{url('category/'.$category->slug.'/'.$product->slug)}}">
                            <div class="card shadow">
                                <img src="{{ asset('assets/uploads/product/'.$product->image) }}" alt="Product image" class="img-responsive img-thumbnail" />
                                <div class="card-body">
                                    <h4>{{$product->name}}</h4>
                                    <span class="float-start">{{$product->selling_price}}</span>
                                    <span class="float-end"><s>{{$product->original_price}}</s></span>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
