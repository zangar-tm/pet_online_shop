@extends('layouts.front')

@section('title')
    My Cart
@endsection

@section('content')
    <div class="py-3 mb-4 shadow-sm bg-warning border-top">
        <div class="container">
            <h6 class="mb-0">
                <a href="{{url('/') }}">
                    Home
                </a> / Cart

            </h6>
        </div>
    </div>
    <div class="container py-5">
        <div class="card shadow">
            <div class="card-body">
                @if(count($cart_products)>0)
                    <div class="row product_data">
                        @foreach ($cart_products as $item)
                            <div class="col-md-2">
                                <img src="{{ asset('assets/uploads/product/'. $item->products->image) }}" height="70px" width="70px" alt="Item photo">
                            </div>
                            <div class="col-md-3 mt-4">
                                <h5><a href="{{ url('category/'.$item->products->category->slug.'/'.$item->products->slug)}}">{{ $item->products->name}}</a></h5>
                            </div>
                            <div class="col-md-3">
                                <input type="hidden" class="product_id" value="{{ $item->product_id }}">
                                @if($item->products->quantity > $item->product_quantity)
                                    <label for="Quantity">Quantity</label>
                                    <div class="input-group text-center mb-3" style = "width:130px;">
                                        <button class="input-group-text decrement-btn">-</button>
                                        <input type="text" name="quantity" class="form-control quantity-input text-center" value="{{$item->product_quantity}}">
                                        <button class="input-group-text increment-btn">+</button>
                                    </div>
                                @else
                                    <h6 class="mt-4">Нет в наличии</h6>
                                {{-- @elseif ($item->products->quantity > 0 && $item->products->quantity < $item->product_quantity)
                                    <h6>Осталось всего {{ $item->products->quantity}}</h6> --}}
                                @endif
                            </div>
                            <div class="col-md-2 mt-4">
                                <h5>${{ $item->products->selling_price}}</h5>
                            </div>
                            <div class="col-md-2 mt-4">
                                <button class="btn btn-danger delete-cart-item">Remove</button>
                            </div>
                        @endforeach
                        <hr>
                        <a href="{{ url('checkout') }}" class="btn btn-primary col-md-2 float-end">Checkout</a>
                    </div>
                @else
                    <h4>No products in cart</h4>
                @endif
            </div>
        </div>
    </div>
@endsection
