@extends('layouts.front')

@section('title')
    Whishlist
@endsection

@section('content')
    <div class="py-3 mb-4 shadow-sm bg-warning border-top">
        <div class="container">
            <h6 class="mb-0">
                <a href="{{url('/') }}">
                    Home
                </a> / Wishlist

            </h6>
        </div>
    </div>
    <div class="container py-5">
        <div class="card shadow">
            <div class="card-body">
                @if($wishlist->count() > 0)
                    @foreach ($wishlist as $item)
                    <div class="row product_data">
                        <div class="col-md-2">
                            <img src="{{ asset('assets/uploads/product/'. $item->products->image) }}" height="70px" width="70px" alt="Item photo">
                        </div>
                        <div class="col-md-2 mt-4">
                            <h5><a href="{{ url('category/'.$item->products->category->slug.'/'.$item->products->slug)}}">{{ $item->products->name}}</a></h5>
                        </div>
                        <div class="col-md-2 mt-4">
                            <h5>${{ $item->products->selling_price}}</h5>
                        </div>
                        <div class="col-md-2">
                            <input type="hidden" class="product_id" value="{{ $item->product_id }}">
                            @if($item->products->quantity > $item->product_quantity)
                                <label for="Quantity">Quantity</label>
                                <div class="input-group text-center mb-3" style = "width:130px;">
                                    <button class="input-group-text decrement-btn">-</button>
                                    <input type="text" name="quantity" class="form-control quantity-input text-center" value="1">
                                    <button class="input-group-text increment-btn">+</button>
                                </div>
                            @else
                                <h6 class="mt-4">Нет в наличии</h6>
                            @endif
                        </div>
                        <div class="col-md-4 mt-4">
                            <button class="btn btn-success addToCartBtn">Add to Cart</button>
                            <button class="btn btn-danger delete-wishlist-item">Remove</button>
                        </div>
                    </div>
                    @endforeach
                @else
                    <h4>There are no products in your wishlist</h4>
                @endif
            </div>
        </div>
    </div>
@endsection
