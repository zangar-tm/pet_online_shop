@extends('layouts.front')

@section('title')
    Checkout Page
@endsection

@section('content')
    <div class="container mt-5">
        <form action="{{ url('place-order')}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <h6>Basic details</h6>
                            <hr>
                            <div class="row checkout-form">
                                <div class="col-md-6 mb-3">
                                    <label for="">First Name</label>
                                    <input type="text" class="form-control firstName" name="fname" placeholder="Enter First Name">
                                    <span id="fname_error"></span>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="">Last Name</label>
                                    <input type="text" class="form-control lastName" name="lname" placeholder="Enter Last Name">
                                    <span id="lname_error"></span>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="">Email address</label>
                                    <input type="text" class="form-control email" name="email" placeholder="Enter Email address">
                                    <span id="email_error"></span>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="">Phone Number</label>
                                    <input type="text" class="form-control phone" name="phone" placeholder="Enter phone number">
                                    <span id="phone_error"></span>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="">Address 1</label>
                                    <input type="text" class="form-control address1" name="address1" placeholder="Enter address 1">
                                    <span id="address1_error"></span>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="">Address 2</label>
                                    <input type="text" class="form-control address2" name="address2" placeholder="Enter address 2">
                                    <span id="address2_error"></span>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="">City</label>
                                    <input type="text" class="form-control city" name="city" placeholder="Enter city">
                                    <span id="city_error"></span>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="">State</label>
                                    <input type="text" class="form-control state" name="state" placeholder="Enter state">
                                    <span id="state_error"></span>
                                </div>
                                <div class="col-md-6">
                                    <label for="">Country</label>
                                    <input type="text" class="form-control country" name="country" placeholder="Enter country">
                                    <span id="country_error"></span>
                                </div>
                                <div class="col-md-6">
                                    <label for="">PostCode</label>
                                    <input type="text" class="form-control postcode" name="postcode" placeholder="Enter postcode">
                                    <span id="postcode_error"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-body">
                            <h6>Order Details</h6>
                            <hr>
                            @if(count($cartItems) > 0)
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Quantity</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($cartItems as $item)
                                            <tr>
                                                <td>{{ $item->products->name}}</td>
                                                <td>{{ $item->product_quantity}}</td>
                                                <td>{{ $item->products->selling_price}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <hr>
                                <button type ="submit" class="col-md-12 btn btn-success">Place Order</button>
                                <button type ="button" class="paypal_btn col-md-12 mt-3 btn btn-primary">Pay with Paypal</button>
                                <div id="paypal-button-container"></div>
                            @else
                                <h5 class="text-center">Empty cart</h5>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
<script src="https://www.paypal.com/sdk/js?client-id={{$client_id}} "></script>
<script>
    paypal.Buttons({
  createOrder: function(data, actions) {
    // This function sets up the details of the transaction, including the amount and line item details.
    return actions.order.create({
      purchase_units: [{
        amount: {
          value: '0.01' //total
        }
      }]
    });
  },
  onApprove: function(data, actions) {
    // This function captures the funds from the transaction.
    return actions.order.capture().then(function(details) {
      // This function shows a transaction success message to your buyer.
    //   alert('Transaction completed by ' + details.payer.name.given_name);


        var firstName = $('firstName').val();
        var lastName =$('lastName').val();
        var email =$('email').val();
        var phone =$('phone').val();
        var address1 =$('address1').val();
        var address2 =$('address2').val();
        var city =$('city').val();
        var state =$('state').val();
        var country =$('country').val();
        var postcode =$('postcode').val();
      $.ajax({
          method: "POST",
          url: "/place-order",
          data: {
              'fname': firstName,
              'lname': lastName,
              'email': email,
              'phone': phone,
              'address1': address1,
              'address2': address2,
              'city': city,
              'state': state,
              'country': country,
              'postcode':postcode,
              'payment_mode':'Paid by Paypal',
              'payment_id': details.id,
          }
      })
    });
  }
}).render('#paypal-button-container');
//This function displays payment buttons on your web page.
</script>
@endsection
