@extends('layouts.front')

@section('title')
    Order Detail
@endsection

@section('content')
    <div class="py-3 mb-4 shadow-sm bg-warning border-top">
        <div class="container">
            <h6 class="mb-0">
                <a href="{{url('my-orders') }}">
                    Orders
                </a> / {{ $order->tracking_num }}
            </h6>
        </div>
    </div>
    <div class="container py-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Order View</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Shipping Details</h3>
                                <label for="">First Name</label>
                                <div class="border p-2">{{$order->fname}}</div>
                                <label for="">Last Name</label>
                                <div class="border p-2">{{$order->lname}}</div>
                                <label for="">Email</label>
                                <div class="border p-2">{{$order->email}}</div>
                                <label for="">Contact Number</label>
                                <div class="border p-2">{{$order->phone}}</div>
                                <label for="">Shipping Address</label>
                                <div class="border p-2">
                                    {{$order->address1}},
                                    {{$order->address2}},
                                    {{$order->city}},
                                    {{$order->state}},
                                    {{$order->country}}
                                </div>
                                <label for="">Postal Code</label>
                                <div class="border p-2">{{$order->postcode}}</div>
                            </div>
                            <div class="col-md-6">
                                <h3>Order Details</h3>
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Quantity</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($order->orderItems as $item)
                                        <tr>
                                            <td>{{ $item->products->name }}</td>
                                            <td>
                                                <img src="{{ asset('assets/uploads/product/'.$item->products->image) }}" width = "50px" alt="Order product image" />
                                            </td>
                                            <td>{{ $item->quantity }}</td>
                                            <td>{{ $item->price }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <h4 class="price float-end">Total Price: {{ $order->total_price}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
