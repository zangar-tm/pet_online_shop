@extends('layouts.front')

@section('title')
    Welcome to Online-shop
@endsection

@section('content')
    @include('layouts.inc.front-slider')
    <div class="py-5">
        <div class="container">
            <div class="row">
                <h4>Featured products</h4>
                <div class="owl-carousel owl-theme">
                    @foreach($trending_products as $product)
                        <div class="item">
                            <div class="card shadow">
                                <img src="{{ asset('assets/uploads/product/'.$product->image) }}" alt="Product image" class="img-responsive img-thumbnail" />
                                <div class="card-body">
                                    <h4>{{$product->name}}</h4>
                                    <span class="float-start">{{$product->selling_price}}</span>
                                    <span class="float-end"><s>{{$product->original_price}}</s></span>
                                </div>
                            </div>
                        </div>  
                    @endforeach         
                </div> 
            </div>
        </div>
    </div>
    <div class="py-5">
        <div class="container">
            <div class="row">
                <h4>Trending categories</h4>
                <div class="owl-carousel owl-theme">
                    @foreach($trending_categories as $tcategory)
                        <a href="{{url('view-category/'.$tcategory->slug)}}">
                            <div class="item">
                                <div class="card shadow">
                                    <img src="{{ asset('assets/uploads/category/'.$tcategory->image) }}" alt="Category image" class="img-responsive img-thumbnail" />
                                    <div class="card-body">
                                        <h4 class="text-decoration-none">{{$tcategory->name}}</h4>
                                        <span class="float-start">{{$tcategory->description}}</span>
                                    </div>
                                </div>
                            </div>  
                        </a>
                    @endforeach         
                </div> 
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>

$(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
    margin: 10,
    nav: true,
    loop: true,
    responsive: {
        0: {
        items: 1
        },
        600: {
        items: 3
        },
        1000: {
        items: 4
        }
    }
    })
})
          
</script>
@endsection()

