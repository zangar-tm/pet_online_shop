<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header text-center pt-4">
        <span class="ms-1 font-weight-bold text-white">Online-shop</span>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto  max-height-vh-100" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item ">
          <a class="nav-link text-white {{ Request::is('dashboard') ? 'active' : '' }}" href="{{ url('dashboard') }}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">Dashboard</i>
            </div>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white {{ Request::is('categories') ? 'active' : '' }}" href="{{ url('categories') }}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">Categories</i>
            </div>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white {{ Request::is('add-category') ? 'active' : ''}}" href="{{ url('add-category') }}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">Add Category</i>
            </div>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white {{ Request::is('products') ? 'active' : ''; }}" href="{{ url('products') }}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">Products</i>
            </div>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white {{ Request::is('add-product') ? 'active' : ''; }}" href="{{ url('add-product') }}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">Add Product</i>
            </div>
          </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white {{ Request::is('orders') ? 'active' : ''; }}" href="{{ url('orders') }}">
              <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                <i class="material-icons opacity-10">Orders</i>
              </div>
            </a>
        </li>
        <li class="nav-item">
        <a class="nav-link text-white {{ Request::is('users') ? 'active' : ''; }}" href="{{ url('users') }}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="material-icons opacity-10">Users</i>
            </div>
        </a>
        </li>


      </ul>
    </div>
  </aside>
