@extends('layouts.admin')

@section('title')
    Orders
@endsection

@section('content')
    <div class="card">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <h4>{{ $message }}</h4>
        </div>
        @endif
        <div class="card-header">
            <h1>Order Page</h1>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Order Date</th>
                        <th>Tracking Number</th>
                        <th>Total Price</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)
                    <tr>
                        <td>{{ $order->created_at }}</td>
                        <td>{{ $order->tracking_num }}</td>
                        <td>{{ $order->total_price }}</td>
                        <td>{{ $order->status== '1' ? 'completed' : 'pending' }}</td>
                        <td>
                            <a href="{{ url('admin/view-order/'.$order->id) }}" class="btn btn-primary">View</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
