@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Add Product</h4>
        </div>
        <div class="card-body">
            <form action="{{ url('insert-product') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-4 mb-3">
                    <label for="name">Category</label>
                        <select class="form-select form-select-lg mb-3" name="category_id" aria-label=".form-select-lg example">
                            <option value="">Select a category</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Enter name">
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="slug">Slug</label>
                        <input type="text" class="form-control" name="slug" placeholder="Enter slug">
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="short_description">Short Description</label>
                        <textarea class="form-control" rows="3" name="short_description" placeholder="Enter short description"></textarea> 
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="description">Description</label>
                        <textarea class="form-control" rows="3" name="description" placeholder="Enter description"></textarea> 
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="original_price">Original Price</label>
                        <input type="number" class="form-control" name="original_price">
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="selling_price">Selling Price</label>
                        <input type="number" class="form-control" name="selling_price">
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="quantity">Quantity</label>
                        <input type="number" class="form-control" name="quantity">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="status">Status</label>
                        <input type="checkbox" name="status">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="trending">Trending</label>
                        <input type="checkbox" name="trending">
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="meta_title">Meta Title</label>
                        <input type="text" class="form-control" name="meta_title" placeholder="Enter meta title">
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="meta_description">Meta Description</label>
                        <textarea class="form-control" rows="3" name="meta_description" placeholder="Enter meta description"></textarea> 
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="meta_keywords">Meta Keywords</label>
                        <textarea class="form-control" rows="3" name="meta_keywords" placeholder="Enter meta keywords"></textarea> 
                    </div>
                    
                    <div class="col-md-12 mb-3">
                        <label for="file">Upload file</label>
                        <input type="file" class="form-control" name="file">
                    </div>

                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
