@extends('layouts.admin')

@section('content')
    <div class="card">
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <h4>{{ $message }}</h4>
        </div>
    @endif
        <div class="card-header">
            <h1>Product Page</h1>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Category</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Selling Price</th>
                        <th>Short Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->category->name }}</td>
                        <td>
                            <img src="{{ asset('assets/uploads/product/'.$product->image) }}" class="product-image" alt="Image here">
                        </td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->selling_price }}</td>
                        <td>{{ $product->short_description }}</td>
                        <td>
                            <a href="{{ url('edit-product/'.$product->id) }}" class="btn btn-primary">Edit</a>
                            <form action="{{ url('delete-product/'. $product->id ) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection