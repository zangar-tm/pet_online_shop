@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <h4>Add Product</h4>
        </div>
        <div class="card-body">
            <form action="{{ url('update-product/'.$product->id) }}" method="POST" enctype="multipart/form-data" role="form">
                @csrf
                @method('PUT')
                <div class="row">
                <div class="col-md-4 mb-3">
                    <label for="name">Category</label>
                        <select class="form-select form-select-lg mb-3" name="category_id" aria-label=".form-select-lg example">
                                <option value="">{{ $product -> category -> name }}</option>
                        </select>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $product->name }}">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="slug">Slug</label>
                        <input type="text" class="form-control" name="slug" value="{{ $product->slug }}">
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="short_description">Short Description</label>
                        <textarea class="form-control" rows="3" name="short_description">{{ $product -> short_description }}</textarea> 
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="description">Description</label>
                        <textarea class="form-control" rows="3" name="description" >{{ $product -> description }}</textarea> 
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="original_price">Original Price</label>
                        <input type="number" class="form-control" name="original_price" value="{{ $product -> original_price }}">
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="selling_price">Selling Price</label>
                        <input type="number" class="form-control" name="selling_price" value="{{ $product ->selling_price }}">
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="quantity">Quantity</label>
                        <input type="number" class="form-control" name="quantity" value="{{ $product ->quantity }}">
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="status">Status</label>
                        <input type="checkbox" name="status" {{ $product->status == '1' ? 'checked' : '' }}>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="trending">Trending</label>
                        <input type="checkbox" name="trending" {{ $product->status=='1' ? 'checked' : '' }}">
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="meta_title">Meta Title</label>
                        <input type="text" class="form-control" name="meta_title" value="{{ $product->meta_title }}">
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="meta_description">Meta Description</label>
                        <textarea class="form-control" rows="3" name="meta_description" >{{ $product->meta_description }}</textarea> 
                    </div>
                    <div class="col-md-12 mb-3">
                        <label for="meta_keywords">Meta Keywords</label>
                        <textarea class="form-control" rows="3" name="meta_keywords">{{ $product->meta_keywords }}</textarea> 
                    </div>
                    @if($product->image)
                        <img src="{{ asset('assets/uploads/product/'.$product->image) }}" alt="product Image">
                    @endif
                    <div class="col-md-12">
                        <label for="file">Upload file</label>
                        <input type="file" class="form-control" name="file">
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection