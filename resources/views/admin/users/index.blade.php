@extends('layouts.admin')

@section('title')
    Users
@endsection

@section('content')
    <div class="card">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <h4>{{ $message }}</h4>
        </div>
        @endif
        <div class="card-header">
            <h1>Registered Users</h1>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <a href="#" class="btn btn-primary">View</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
