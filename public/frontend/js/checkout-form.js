$(document).ready(function(){
    $('.paypal_btn').click(function(){
        e.preventDefault();

        alert('hello');
        var firstName = $('firstName').val();
        var lastName =$('lastName').val();
        var email =$('email').val();
        var phone =$('phone').val();
        var address1 =$('address1').val();
        var address2 =$('address2').val();
        var city =$('city').val();
        var state =$('state').val();
        var country =$('country').val();
        var postcode =$('postcode').val();

        if(!firstName){
            fname_error = "First Name required";
            $('#fname_error').html('');
            $('#fname_error').val(fname_error);
        } else{
            fname_error = "";
            $('#fname_error').html('');
        }

        if(!lastName){
            lname_error = "Last Name required";
            $('#lname_error').html('');
            $('#lname_error').val(lname_error);
        } else{
            lname_error = "";
            $('#lname_error').html('');
        }

        if(!email){
            email_error = "Email required";
            $('#email_error').html('');
            $('#email_error').val(email_error);
        } else{
            email_error = "";
            $('#email_error').html('');
        }

        if(!phone){
            phone_error = "Phone number required";
            $('#phone_error').html('');
            $('#phone_error').val(phone_error);
        } else{
            phone_error = "";
            $('#phone_error').html('');
        }

        if(!city){
            city_error = "City required";
            $('#city_error').html('');
            $('#city_error').val(city_error);
        } else{
            city_error = "";
            $('#city_error').html('');
        }

        if(!state){
            state_error = "State required";
            $('#state_error').html('');
            $('#state_error').val(state_error);
        } else{
            state_error = "";
            $('#state_error').html('');
        }

        if(!country){
            country_error = "Country required";
            $('#country_error').html('');
            $('#country_error').val(country_error);
        } else{
            country_error = "";
            $('#country_error').html('');
        }

        if(!postcode){
            postcode_error = "Postcode required";
            $('#postcode_error').html('');
            $('#postcode_error').val(postcode_error);
        } else{
            postcode_error = "";
            $('#postcode_error').html('');
        }

        if(!address1){
            address1_error = "Address 1 required";
            $('#address1_error').html('');
            $('#address1_error').val(address1_error);
        } else{
            address1_error = "";
            $('#address1_error').html('');
        }

        if(!address2){
            address2_error = "Address 2 required";
            $('#address2_error').html('');
            $('#address2_error').val(address2_error);
        } else{
            address2_error = "";
            $('#address2_error').html('');
        }

        if(fname_error !='' || lname_error !='' || email_error !='' || address1_error !='' || address2_error !='' || phone_error !='' || city_error != '' || state_error != '' || country_error != '' || postcode_error != '') {
            return false;
        } else{
            var data = {
                'firstName' : firstName,
                'lastName' : lastName,
                'email' : email,
                'phone' :phone,
                'address1' : address1,
                'address2' : address2,
                'city' :city,
                'state' :state,
                'country' :country,
                'postcode' :postcode
            };

            $.ajax({
                type: "POST",
                url: "/proceed-to-pay",
                data: data,
                success: function(response){

                }
            });
        }
    });
});
