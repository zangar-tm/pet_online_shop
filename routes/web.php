<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Admin\FrontendController;
use App\Http\Controllers\Frontend\UserFrontendController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\CheckoutController;
use App\Http\Controllers\Frontend\UserController;
use App\Http\Controllers\Frontend\WishlistController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\RegisteredUsersController;
use App\Http\Controllers\HomeController;


Auth::routes();
Route::get('/home', [HomeController::class, 'index'])->name('home');


Route::get('/', [UserFrontendController::class, 'index']);
Route::get('/category', [UserFrontendController::class, 'category']);
Route::get('category/{cate_slug}', [UserFrontendController::class, 'viewcategory']);
Route::get('category/{cate_slug}/{prod_slug}', [UserFrontendController::class, 'viewproduct']);
Route::post('add-to-wishlist',[WishlistController::class, 'addwishProd']);
Route::post('add-to-cart',[CartController::class, 'addProduct']);

Route::post('delete-cart-item', [CartController::class, 'deleteProduct']);
Route::post('delete-wishlist-item', [WishlistController::class, 'deletewishProduct']);
Route::middleware('auth')->group(function () {
    Route::get('cart',[CartController::class, 'viewCart']);
    Route::get('checkout',[CheckoutController::class, 'index']);
    Route::post('place-order', [CheckoutController::class, 'placeorder']);
    Route::get('my-orders',[UserController::class, 'index']);
    Route::get('view-order/{id}',[UserController::class, 'viewOrder']);
    Route::get('wishlist', [WishlistController::class, 'index']);
    Route::post('proceed-to-pay', [CheckoutController::class,'paypalcheck']);
});


Route::middleware(['auth','isAdmin'])->group(function () {
    Route::get('dashboard', [FrontendController::class, 'index']);

    //  Categories Routes
    Route::get('categories', [CategoryController::class, 'index']);
    Route::get('add-category', [CategoryController::class, 'add']);
    Route::post('insert-category', [CategoryController::class, 'insert']);
    Route::get('edit-category/{category}', [CategoryController::class, 'edit']);
    Route::put('update-category/{category}', [CategoryController::class, 'update']);
    Route::delete('delete-category/{category}', [CategoryController::class, 'delete']);

    //  Products Routes
    Route::get('products', [ProductController::class, 'index']);
    Route::get('add-product', [ProductController::class, 'add']);
    Route::post('insert-product', [ProductController::class, 'insert']);
    Route::get('edit-product/{product}', [ProductController::class, 'edit']);
    Route::put('update-product/{product}', [ProductController::class, 'update']);
    Route::delete('delete-product/{product}', [ProductController::class, 'delete']);

    Route::get('users', [RegisteredUsersController::class, 'index']);

    Route::get('orders', [OrderController::class, 'index']);
    Route::get('admin/view-order/{id}', [OrderController::class, 'show']);
    Route::put('update-order/{id}',[OrderController::class, 'update']);

});
