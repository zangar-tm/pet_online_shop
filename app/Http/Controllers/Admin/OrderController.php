<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
class OrderController extends Controller
{
    public function index(){
        $orders = Order::where('status','0')->get();
        return view('admin.orders.index', compact('orders'));
    }

    public function show($id){
        $order = Order::find($id);
        return view('admin.orders.show', compact('order'));
    }

    public function update(Request $request, $id){
        $order = Order::find($id);
        $order->status = $request->input('order_status');
        $order->update();

        return redirect('orders')->with('status','Order updated successfully');
    }
}
