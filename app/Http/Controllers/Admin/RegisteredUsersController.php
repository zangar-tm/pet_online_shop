<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
class RegisteredUsersController extends Controller
{
    public function index(){
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }

    // public function show($id){
    //     $user = User::find($id);
    //     return view('admin.users.show', compact('user'));
    // }
}
