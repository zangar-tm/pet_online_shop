<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
class CategoryController extends Controller
{
    public function index(){
        $category = Category::all();
        return view('admin.category.index', compact('category'));
    }

    public function add(){
        return view('admin.category.add');
    }

    public function insert(Request $request){
        $category = new Category();
        if($request->hasFile('file'))
        {
            $extension = $request->file->extension();
            $filename = time().'.'.$extension;
            $request->file->move('assets/uploads/category',$filename);
            $category->image = $filename;
        }
        $category->name = $request->input('name');
        $category->slug = $request->input('slug');
        $category->description = $request->input('description');
        $category->status = $request->input('status') ==TRUE ? '1':'0';
        $category->popular = $request->input('popular') ==TRUE ? '1':'0';
        $category->meta_title = $request->input('meta_title');
        $category->meta_description = $request->input('meta_description');
        $category->meta_keywords = $request->input('meta_keywords');
        $category->save();
        return redirect('/categories')->with('success','Category created successfully');
    }

    public function edit($categoryId){
        $category = Category::find($categoryId);
        return view('admin.category.edit', compact('category'));
    }

    public function update(Request $request, $categoryId){
        $category = Category::find($categoryId);
        if($request->hasFile('image')){
            $path = 'assets/uploads/category/'.$category->image;
            if(File::exists($path)){
                File::delete($path);
            }
            $file = $request->file('image');
            $extension = $file->extension();
            $filename = time().'.'.$extension;
            $file->move('assets/uploads/category/',$filename);
            $category->image = $filename;
        }
        $category->name = $request->input('name');
        $category->slug = $request->input('slug');
        $category->description = $request->input('description');
        $category->status = $request->input('status') ==TRUE ? '1':'0';
        $category->popular = $request->input('popular') ==TRUE ? '1':'0';
        $category->meta_title = $request->input('meta_title');
        $category->meta_description = $request->input('meta_description');
        $category->meta_keywords = $request->input('meta_keywords');
        $category->update();
        return redirect('/categories')->with('success', "Category Updated Successfully");
    }

    public function delete($categoryId){
        $category = Category::find($categoryId);

        if($category->image){
            $path = 'assets/uploads/category/'.$category->image;
            if(File::exists($path)){
                File::delete($path);
            }
        }
        $category->delete();
        return redirect('/categories')->with('success', "Category Deleted Successfully");
    }
}
