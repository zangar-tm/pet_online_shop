<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\File;
class ProductController extends Controller
{
    public function index(){
        $products = Product::all();
        return view('admin.product.index', compact('products'));
    }

    public function add(){
        $categories = Category::all();
        return view('admin.product.add', compact('categories'));
    }

    public function insert(Request $request){
        $product = new Product();
        if ($request->hasfile('file')){
            $extension = $request->file->extension();
            $filename = time().'.'.$extension;
            $request->file->move('assets/uploads/product',$filename);
            $product->image = $filename;
        }
        $product->category_id = $request->input('category_id');
        $product->name = $request->input('name');
        $product->slug = $request->input('slug');
        $product->short_description = $request->input('short_description');
        $product->description = $request->input('description');
        $product->original_price = $request->input('original_price');
        $product->selling_price = $request->input('selling_price');
        $product->quantity = $request->input('quantity');
        $product->status = $request->input('status') ==TRUE ? '1':'0';
        $product->trending = $request->input('trending') ==TRUE ? '1':'0';
        $product->meta_title = $request->input('meta_title');
        $product->meta_description = $request->input('meta_description');
        $product->meta_keywords = $request->input('meta_keywords');
        $product->save();
        return redirect('/products')->with('success','Product created successfully');
    }

    public function edit($productId){
        $product = Product::find($productId);
        return view('admin.product.edit', compact('product'));
    }

    public function update(Request $request, $productId){
        $product = Product::find($productId);

        if($request->hasFile('image')){
            $path = 'assets/uploads/category/'.$product->image;
            if(File::exists($path)){
                File::delete($path);
            }
            $extension = $request->file->extension();
            $filename = time().'.'.$extension;
            $request->file->move('assets/uploads/product',$filename);
            $product->image = $filename;
        }

        $product->name = $request->input('name');
        $product->slug = $request->input('slug');
        $product->short_description = $request->input('short_description');
        $product->description = $request->input('description');
        $product->original_price = $request->input('original_price');
        $product->selling_price = $request->input('selling_price');
        $product->quantity = $request->input('quantity');
        $product->status = $request->input('status') ==TRUE ? '1':'0';
        $product->trending = $request->input('trending') ==TRUE ? '1':'0';
        $product->meta_title = $request->input('meta_title');
        $product->meta_description = $request->input('meta_description');
        $product->meta_keywords = $request->input('meta_keywords');
        $product->update();
        return redirect('/products')->with('success', "Product Updated Successfully");
    }

    public function delete($productId){
        $product = Product::find($productId);

        if($product->image){
            $path = 'assets/uploads/product/'.$product->image;
            if(File::exists($path)){
                File::delete($path);
            }
        }
        $product->delete();
        return redirect('/products')->with('success', "Product Deleted Successfully");
    }
}
