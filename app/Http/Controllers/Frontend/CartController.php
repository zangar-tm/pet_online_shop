<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Cart;
use Illuminate\Support\Facades\Auth;
class CartController extends Controller
{
    public function addProduct(Request $request){

        $product_id = $request->input('product_id');
        $product_quantity = $request->input('product_quantity');

        if(Auth::check())
        {
            $product_check = Product::where('id', $product_id)->first();

            if($product_check)
            {
                if(Cart::where('product_id', $product_id)->where('user_id', Auth::id())->exists()){
                    return response()->json(['status' => $product_check->name. "Already added to cart"]);
                }
                $cartItem = new Cart();
                $cartItem->product_id = $product_id;
                $cartItem->user_id = Auth::id();
                $cartItem->product_quantity = $product_quantity;
                $cartItem->save();
                return response()->json(['status' => $product_check->name." added to cart"]);
            }
        }
        return response()->json(['status' =>"login to continue"]);
    }

    public function viewCart(){
        $cart_products = Cart::where('user_id', Auth::id())->get();
        return view('frontend.cart', compact('cart_products'));
    }

    public function deleteProduct(Request $request){
        if(Auth::check()){
            $product_id = $request->input('product_id');
            if(Cart::where('user_id', Auth::id())->where('product_id', $product_id)->exists()){
                $cartItem = Cart::where('user_id', Auth::id())->where('product_id', $product_id)->first();
                $cartItem->delete();
                return response()->json(['status' =>"Successfully deleted"]);
            }
        }
        return response()->json(['status' =>"Login to continue"]);
    }
}
