<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
class CheckoutController extends Controller
{
    public function index(){
        $old_cartItems = Cart::where('user_id', Auth::id())->get();
        foreach ($old_cartItems as $item){
            if(!Product::where('id', $item->product_id)->where('quantity', '>=', $item->product_quantity)->exists()) {
                $remoteItem = Cart::where('user_id', Auth::id())->where('product_id', $item->product_id)->first();
                $remoteItem->delete();
            }
        }
        $client_id = env('PAYPAL_CLIENT_ID');
        $cartItems = Cart::where('user_id', Auth::id())->get();

        return view('frontend.checkout', compact(['cartItems','client_id']));
    }

    public function placeorder(Request $request){
        $order = new Order();
        $order->user_id = Auth::id();
        $order->fname = $request->input('fname');
        $order->lname = $request->input('lname');
        $order->email = $request->input('email');
        $order->address1 = $request->input('address1');
        $order->address2 = $request->input('address2');
        $order->phone = $request->input('phone');
        $order->city = $request->input('city');
        $order->state = $request->input('state');
        $order->country = $request->input('country');
        $order->postcode = $request->input('postcode');
        $order->tracking_num = 'ZTM'.rand(1111, 9999);

        $total = 0;
        $cartItems_total = Cart::where('user_id', Auth::id())->get();
        foreach ($cartItems_total as $product){
            $total +=$product->products->selling_price;
        }

        $order->total_price = $total;
        $order->save();

        $cartItems = Cart::where('user_id', Auth::id())->get();
        foreach ($cartItems as $item){
            OrderItem::create([
                'order_id' => $order->id,
                'product_id' =>$item->product_id,
                'quantity' => $item->product_quantity,
                'price' => $item->products->selling_price,
            ]);
        }

        if($request->input('payment_mode') == 'Paid by Paypal'){
            return response()->json(['status'=>"Order placed Successfully"]);
        }
        return redirect('/')->with('status', "Order placed Successfully");
    }

    public function paypalcheck(Request $request){
        //
    }
}
