<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
class UserFrontendController extends Controller
{
    public function index(){
        $trending_products = Product::where('trending','1')->take(15)->get();
        $trending_categories = Category::where('popular','1')->take(15)->get();
        return view('frontend.index', compact('trending_products', 'trending_categories'));
    }

    public function category(){
        $categories = Category::where('status','0')->get();
        return view('frontend.category', compact('categories'));
    }

    public function viewcategory($slug){
        if(Category::where('slug', $slug)->exists())
         {
            $category = Category::where('slug',$slug)->first();
            $products = Product::where('category_id', $category->id)->get();
            return view('frontend.products.index', compact('category','products'));
        } else{
            return redirect('/')->with('status', "Category does not exist");
        }
    }

    public function viewproduct($cate_slug, $prod_slug){
        if(Category::where('slug',$cate_slug)->exists())
        {
            if(Product::where('slug',$prod_slug)->exists())
            {
                $product = Product::where('slug',$prod_slug)->first();
                return view('frontend.products.show',compact('product'));
            }
            return redirect('/')->with('status', "Product does not exist");

        }
        return redirect('/')->with('status', "Category does not exist");


    }
}
