<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Wishlist;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
class WishlistController extends Controller
{
    public function index(){
        $wishlist = Wishlist::where('user_id', Auth::id())->get();
        return view('frontend.wishlist', compact('wishlist'));
    }

    public function addwishProd(Request $request){
        $product_id = $request->input('product_id');

        if(Auth::check())
        {
            $product_check = Product::where('id', $product_id)->first();
            if($product_check)
            {
                if(Wishlist::where('product_id', $product_id)->where('user_id', Auth::id())->exists())
                {
                    return response()->json(['status' => $product_check->name. "Already added to wishlist"]);
                }
                $wishItem = new Wishlist();
                $wishItem->product_id = $product_id;
                $wishItem->user_id = Auth::id();
                $wishItem->save();
                return response()->json(['status' => $product_check->name." added to wishlist"]);
            }
        }
        return response()->json(['status' =>"login to continue"]);
    }

    public function deletewishProduct(Request $request){
        if(Auth::check()){
            $product_id = $request->input('product_id');
            if(Wishlist::where('user_id', Auth::id())->where('product_id', $product_id)->exists()){
                $wishItem = Wishlist::where('user_id', Auth::id())->where('product_id', $product_id)->first();
                $wishItem->delete();
                return response()->json(['status' =>"Successfully deleted"]);
            }
        }
        return response()->json(['status' =>"Login to continue"]);
    }
}
