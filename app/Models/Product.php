<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $table = 'products';

    protected $fillable = [
        'category_id',
        'name',
        'slug',
        'short_description',
        'description',
        'original_price',
        'selling_price',
        'quantity',
        'image',
        'status',
        'trending',
        'meta_title',
        'meta_keywords',
        'meta_description',
        
    ];
    use HasFactory;

    public function category(){
        return $this->belongsTo(Category::class); // ,'category_id','id'
    }
}
